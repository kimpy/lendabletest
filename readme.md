## Setting up the Environment

The solution is implement in Python 3.6 and uses  built in modules.

## Running the Code

Clone the repo and navigate to the Solution folder i.e ` cd ./Solution`
Inside there is a folder called `tests` with the test csv files more csv files can be added here.
The main file with the required function `best_credit_scorers` is `customers_score.py`

To run the file, on a console with python3 installed run `Python3 ./customers_score.py` when inside the Solution folder.

