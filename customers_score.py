import csv
import datetime, time
from collections import OrderedDict
from operator import itemgetter

def  best_credit_scorers(file_name,number_of_customers):
    """
    Returns best number_of_customers records in file_name CSV.
    """
    with open(file_name, newline='') as client_records:
        reader = csv.DictReader(client_records)
        # sort by client and for each client create a score
        # save it in a sorted list and return the first number_of_customers
        final_list = []
        client_scores = {}
        sortedlist = sorted(reader, key=lambda row:(row['customer_id'], row['transaction_date']))
        previous_record = None
        for row in sortedlist:
            customer_score = client_scores.get(row['customer_id'])
            if customer_score is None: # New record
                client_scores.update({row['customer_id']: 0}) # Assume  a score of zero
            else:
                if previous_record and (previous_record['customer_id']==row['customer_id']):
                    delta = (datetime.datetime(*time.strptime(row['transaction_date'], "%Y-%m-%d %H:%M:%S")[:6])-
                        datetime.datetime(*time.strptime(previous_record['transaction_date'], "%Y-%m-%d %H:%M:%S")[:6])).days
                    if delta == 1:
                        client_scores[row['customer_id']] += 1
            previous_record = row

        # return the number of customers
        sorted_scores = OrderedDict(sorted(sorted(client_scores.items(), key = itemgetter(0), reverse=True),key=itemgetter(1), reverse = True))
        previous_acc = None
        for x, y in sorted_scores.items():
            if len(final_list) == number_of_customers:
                break
            else:
                if previous_acc is None:
                    previous_acc = (x, y)
                    final_list.append(x)
                if previous_acc[1] != y:
                    final_list.append(x)
                else:
                    final_list.remove(previous_acc[0])
                    final_list.append(x)
                previous_acc = (x, y)
        return final_list


if __name__ == "__main__":
    # Testing the results
    print(best_credit_scorers("./tests/transaction_data_1.csv", 1))
    print(best_credit_scorers("./tests/transaction_data_2.csv", 2))
    print(best_credit_scorers("./tests/transaction_data_3.csv", 3))
